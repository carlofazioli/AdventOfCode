Main repository for [Advent Of Code](https://adventofcode.com).

# Usage

This repo isn't really for anyone else.  However if you wanna copy the
input-fetching script then all this is required is that you pull you
AoC session token out of your browser cookies after logging in.  

Namely, log in via the web.  Then, open up your browser's web console
(usually something like CTRL+SHIFT+I) and click on, say, the AoC home
page.  In the Network tab you should see a `session` under "cookie". 
Paste *just the value* into a file called `.aoc_token` in your project
root dir.  Assuming you `chmod +x` the `aoc_get_input.py` file, you can 
just run it each day after midnight to automatically fetch that day's 
input.  

# Note

Because I am continually updating my process, it's probably the case 
that only the current year's code works.  Previous years probably have
stale python imports to local files that may no longer exist.  
