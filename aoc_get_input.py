#!/usr/bin/env python

# Built-ins:
from datetime import datetime
from pathlib import Path
import sys

# Installed:
import requests


# Set defaults:
PROJECT_ROOT = Path('/home/carlo/AdventOfCode')
now = datetime.now()
YEAR = now.year
DAY = now.day
with open(f'{PROJECT_ROOT}/.aoc_token', 'r') as f:
    aoc_token = f.read().strip()
AOC_COOKIE = {'session': aoc_token}

# Locations:
URL = f'https://adventofcode.com/{YEAR}/day/{DAY}'
DEST = Path(f'{PROJECT_ROOT}/events/{YEAR}/input_data/day_{DAY}.txt')

# Check for file:
if DEST.exists():
    print('File already exists. Exiting...')
    sys.exit()

# Download:
print('No local input file. Downloading...')
response = requests.get(f'{URL}/input', cookies=AOC_COOKIE)
if response.ok:
    with open(DEST, 'w') as f:
        f.write(response.text)
    print('...done')
else:
    print('HTTP error!')
