#!/usr/bin/env python

'''
The idea behind this program is that it will grab the AoC input file
automatically at release time.  

It does that by determining the date at launch time (ideally that is
sometime before midnight), and then waiting until after midnight.  
After midnight, it grabs the file and exits.  
'''

# Built-ins:
from datetime import datetime
from pathlib import Path
import sys
from time import sleep

# Installed:
import requests


# Set defaults:
PROJECT_ROOT = Path('/home/carlo/AdventOfCode')
with open(f'{PROJECT_ROOT}/.aoc_token', 'r') as f:
    aoc_token = f.read().strip()
AOC_COOKIE = {'session': aoc_token}
YEAR = datetime.now().year
DAY = datetime.now().day
MIDNIGHT = datetime(YEAR, 12, DAY+1)

# Locations:
URL = f'https://adventofcode.com/{YEAR}/day/{DAY}'
DEST = Path(f'{PROJECT_ROOT}/events/{YEAR}/{DAY}.in')

# Check for file:
if DEST.exists():
    print('File already exists. Exiting...')
    sys.exit()

# Download:
print('No local input file. Waiting to download...')
while datetime.now() < MIDNIGHT:
    sleep(1)
    
response = requests.get(f'{URL}/input', cookies=AOC_COOKIE)
if response.ok:
    with open(DEST, 'w') as f:
        f.write(response.text)
    print('...done')
else:
    print('HTTP error!')
