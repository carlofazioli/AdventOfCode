from utilities import load_data, submit

data = load_data()
data = data.split('\n')

score = 0

for game in data:
    a,b = game.split(' ')
    if a == 'A' and b == 'X':
        score += 1
        score += 3
    elif a == 'A' and b == 'Y':
        score += 2
        score += 6
    elif a == 'A' and b == 'Z':
        score += 3
        score += 0
    elif a == 'B' and b == 'X':
        score += 1
        score += 0
    elif a == 'B' and b == 'Y':
        score += 2
        score += 3
    elif a == 'B' and b == 'Z':
        score += 3
        score += 6
    elif a == 'C' and b == 'X':
        score += 1
        score += 6
    elif a == 'C' and b == 'Y':
        score += 2
        score += 0
    elif a == 'C' and b == 'Z':
        score += 3
        score += 3


r = submit(score, 1)

print(score)

score = 0

for game in data:
    a,b = game.split(' ')
    if a == 'A' and b == 'X':
        # they played rock and i lose => i played scissors
        score += 3
        score += 0
    elif a == 'A' and b == 'Y':
        # they played rock and we draw => i played rock
        score += 1
        score += 3
    elif a == 'A' and b == 'Z':
        # they played rock and i win => i played paper
        score += 2
        score += 6
    elif a == 'B' and b == 'X':
        # they played paper and i lose => i played rock
        score += 1
        score += 0
    elif a == 'B' and b == 'Y':
        # they played paper and we tie => i played paper
        score += 2
        score += 3
    elif a == 'B' and b == 'Z':
        # they played paper and i win => i played scissors
        score += 3
        score += 6
    elif a == 'C' and b == 'X':
        # they played scissors and i lose => i played paper
        score += 2
        score += 0
    elif a == 'C' and b == 'Y':
        # they played scissors and we tie => i played scissors
        score += 3
        score += 3
    elif a == 'C' and b == 'Z':
        # they played scissors and i win => i played rock
        score += 1
        score += 6

print(score) 
r = submit(score, 2)
