from utilities import load_data, submit

data = load_data()
data = data.split('\n')
answer = 0

def fully_contained(pair):
    a, b = map(int, pair[0].split('-'))
    c, d = map(int, pair[1].split('-'))
    if a <= c and b >= d:
        return True
    if c <= a and d >= b:
        return True
    return False

for line in data:
    answer += fully_contained(line.split(','))

print(answer)
# r = submit(answer, 1)

def overlap(pair):
    a, b = map(int, pair[0].split('-'))
    c, d = map(int, pair[1].split('-'))
    if a <= c and c <= b:
        return True
    if a <= d and c <= b:
        return True
    if c <= a and a <= d:
        return True
    if c <= b and b <= d:
        return True
    return False

answer = 0
for line in data:
    answer += overlap(line.split(','))

print(answer)
r = submit(answer, 2)
