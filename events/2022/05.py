from utilities import load_data, submit

data = load_data()
_, procedure = data.split('\n\n')
procedure = procedure.split('\n')
# print(procedure)

stacks = [
    'BVSNTCHQ',
    'WDBG',
    'FWRTSQB',
    'LGWSZJDN',
    'MPDVF',
    'FWJ',
    'LNQBJV',
    'GTRCJQSN',
    'JSQCWDM',
]

MAP = list(map(list, stacks))


def update_map(rule):
    _, n, _, src, _, dst = rule.split(' ')
    n, src, dst = list(map(int, [n, src, dst]))
    src -= 1
    dst -= 1

    '''
    print(n, src, dst)
    print(MAP)
    print()
    '''

    for _ in range(n):
        vec = MAP[src]
        k = len(vec)
        stack, box = vec[:k-1], vec[-1]
        MAP[src] = stack
        MAP[dst] = MAP[dst] + [box]
        '''
        print(MAP)
        print()
        input()
        '''

for rule in procedure:
    update_map(rule)

print(MAP)

answer = ''.join([stack[-1] for stack in MAP if stack])
    


print(answer)
# r = submit(answer, 1)



MAP = list(map(list, stacks))

def update_map_2(rule):
    _, n, _, src, _, dst = rule.split(' ')
    n, src, dst = list(map(int, [n, src, dst]))
    src -= 1
    dst -= 1
    '''
    print(n, src, dst)
    print(MAP)
    print()
    '''

    vec = MAP[src]
    k = len(vec)
    stack, box = vec[:k-n], vec[k-n:]
    MAP[src] = stack
    MAP[dst] = MAP[dst] + box
    '''
    print(MAP)
    print()
    input()
    '''


for rule in procedure:
    update_map_2(rule)

print(MAP)

answer = ''.join([stack[-1] for stack in MAP if stack])
    
