from aoc import *


data = get_data(2022, 7)
data = data.split('\n')
sample_data = '''$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k'''
 
sample_data = sample_data.split('\n')

loc = None
loc_stack = []
dir_map = {}
dir_paths = []
for row in data:
    if row[0] == '$':
        if row[2:4] == 'cd':
            _, _, loc = row.split(' ')
            if loc == '..':
                loc_stack.pop()
                continue
            else:
                loc_stack.append(loc)
        else:
            where = dir_map
            for d in loc_stack[:-1]:
                where = where[d]
            if loc not in where:
                where[loc] = {}
            dir_paths.append(list(loc_stack))
    else:
        a, name = row.split(' ')
        where = dir_map
        for d in loc_stack:
            where = where[d]
        if a.isdigit():
            where[name] = int(a)
        else:
            where[name] = {}



def mysize(d):
    s = 0
    for k, v in d.items():
        if isinstance(v, dict):
            s += mysize(v)
        else:
            s += v
    return s


sizes_another_way = {}
for path in dir_paths:
    where = dir_map
    for p in path:
        where = where[p]
    sizes_another_way[path[-1]] = mysize(where)

total = mysize(dir_map)

print(total)

avail = 70000000
need = 30000000
unused = avail - total
deficit = need - unused

smallest = 100000000
for d, s in sizes_another_way.items():
    if s >= deficit and s < smallest:
        smallest = s

print(smallest)



x = 1