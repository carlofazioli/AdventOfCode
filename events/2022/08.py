

with open('./input_data/day_8.txt', 'r') as f:
    data = f.read().strip().split('\n')


sample_data = '''30373
25512
65332
33549
35390'''.strip().split('\n')

# data = sample_data

l = len(data) # rows
w = len(data[0]) # cols

for i in range(l):
    data[i] = list(map(int, list(data[i])))

answer = 2*(l+(w-2))

for i in range(1, l-1):
    for j in range(1, w-1):
        # Interior points only
        h = data[i][j]

        visible = True
        for k in range(0, i):
            # From above
            visible &= (data[k][j] < h)
        if visible:
            answer += 1
            continue

        visible = True
        for k in range(i+1, l):
            # From below
            visible &= (data[k][j] < h)
        if visible:
            answer += 1
            continue

        visible = True
        for k in range(0, j):
            # From left
            visible &= (data[i][k] < h)
        if visible:
            answer += 1
            continue

        visible = True
        for k in range(j+1, w):
            # From right
            visible &= (data[i][k] < h)
        if visible:
            answer += 1
            continue

print(answer)

max_scenic = -1
for i in range(1, l-1):
    for j in range(1, w-1):

        h = data[i][j]
        scenic = 1
        # up
        column = [data[k][j] for k in range(i-1, -1, -1)]
        count = 0
        for val in column:
            if val < h:
                count += 1
            else:
                count += 1
                break
        scenic *= count

        # down
        column = [data[k][j] for k in range(i+1, l)]
        count = 0
        for val in column:
            if val < h:
                count += 1
            else:
                count += 1
                break
        scenic *= count

        # left
        column = [data[i][k] for k in range(j-1, -1, -1)]
        count = 0
        for val in column:
            if val < h:
                count += 1
            else:
                count += 1
                break
        scenic *= count

        # RIGHT
        column = [data[i][k] for k in range(j+1, w)]
        count = 0
        for val in column:
            if val < h:
                count += 1
            else:
                count += 1
                break
        scenic *= count

        if scenic > max_scenic:
            max_scenic = scenic


print(max_scenic)

x = 1