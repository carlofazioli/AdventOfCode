

with open('./input_data/day_9.txt', 'r') as f:
    data = f.read().strip()


data = data.split('\n')

f = lambda x: x * 2  # NOQA

'''
H = (0, 0)
T = (0, 0)

tail_locs = set()
tail_locs.add(T)
for row in data:
    direction, dist = row.split(' ')
    for _ in range(int(dist)):
        if direction == 'U':
            H = (H[0], H[1] + 1)
        if direction == 'D':
            H = (H[0], H[1] - 1)
        if direction == 'R':
            H = (H[0] + 1, H[1])
        if direction == 'L':
            H = (H[0] - 1, H[1])
        if abs(H[1] - T[1]) > 1:
            if direction == 'U':
                T = (H[0], H[1]-1)
            if direction == 'D':
                T = (H[0], H[1]+1)
        if abs(H[0] - T[0]) > 1:
            if direction == 'R':
                T = (H[0]-1, H[1])
            if direction == 'L':
                T = (H[0]+1, H[1])
        tail_locs.add(T)

answer = len(tail_locs)
print(answer)

'''

knots = [
    (0, 0),
    (0, 0),
    (0, 0),
    (0, 0),
    (0, 0),
    (0, 0),
    (0, 0),
    (0, 0),
    (0, 0),
    (0, 0),
]

tail_locs = set()
tail_locs.add(knots[-1])
for row in data:
    direction, dist = row.split(' ')
    for _ in range(int(dist)):
        if direction == 'U':
            knots[0] = (knots[0][0], knots[0][1] + 1)
        if direction == 'D':
            knots[0] = (knots[0][0], knots[0][1] - 1)
        if direction == 'R':
            knots[0] = (knots[0][0] + 1, knots[0][1])
        if direction == 'L':
            knots[0] = (knots[0][0] - 1, knots[0][1])
        for k in range(1, 10):
            dx = knots[k][0] - knots[k-1][0]
            dy = knots[k][1] - knots[k-1][1]
            if abs(dx) > 1 and dy == 0:
                knots[k] = (knots[k][0] - int(dx/abs(dx)), knots[k][1])
            if abs(dy) > 1 and dx == 0:
                knots[k] = (knots[k][0], knots[k][1] - int(dy/abs(dy)))
            if abs(dx) > 1 and abs(dy) == 1:
                knots[k] = (knots[k][0] - int(dx/abs(dx)), knots[k][1] - int(dy/abs(dy)))
            if abs(dx) == 1 and abs(dy) > 1:
                knots[k] = (knots[k][0] - int(dx/abs(dx)), knots[k][1] - int(dy/abs(dy)))
            if abs(dx) > 1 and abs(dy) > 1:
                knots[k] = (knots[k][0] - int(dx / abs(dx)), knots[k][1] - int(dy / abs(dy)))
        tail_locs.add(knots[-1])

answer = len(tail_locs)
print(answer)
x = 1


'''




'''