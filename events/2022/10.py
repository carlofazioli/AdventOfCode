with open('./input_data/day_10.txt', 'r') as f:
    data = f.read().strip().split('\n')

x = 1
cycle = 1
strength = 0
for row in data:
    op = row.split(' ')

    for _ in range(len(op)):
        if cycle % 40 == 20:
            strength += x * cycle
        cycle += 1

    if len(op) == 2:
        x += int(op[1])

print(strength)




screen = ''

x = 1
cycle = 1
pos = 0
for row in data:
    op = row.split(' ')
    for _ in range(len(op)):
        if pos % 40 == 0:
            screen += '\n'

        if pos%40 in [x-1, x, x+1]:
            screen += '#'
        else:
            screen += '.'

        pos += 1
        cycle += 1
    if len(op) == 2:
        x += int(op[1])

print(screen)
