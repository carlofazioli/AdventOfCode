with open('./input_data/day_11.txt', 'r') as f:
    data = f.read().strip()

data = data.split('\n\n')


items = {}
activity = {}
for idx, monkey in enumerate(data):
    rows = monkey.split('\n')
    idx = int(rows[0].split(' ')[1][0])
    inventory = list(map(int, rows[1].split(':')[1].split(',')))
    items[idx] = inventory
    activity[idx] = 0

def round():
    for idx, monkey in enumerate(data):
        rows = monkey.split('\n')
        op = rows[2].split(' ')[-2:]
        while items[idx]:
            worry = items[idx].pop(0)
            if op[1] == 'old':
                worry **= 2
            elif op[0] == '+':
                worry += int(op[1])
            else:
                worry *= int(op[1])
            worry //= 3
            divisor = int(rows[3].split(' ')[-1])
            if worry % divisor == 0:
                dest = int(rows[4].split(' ')[-1])
            else:
                dest = int(rows[5].split(' ')[-1])
            items[dest].append(worry)
            activity[idx] += 1

for _ in range(20):
    round()

# print(activity)

items = {}
activity = {}
ops = {}
divisors = {}
dest_true = {}
dest_false = {}
for idx, monkey in enumerate(data):
    rows = monkey.split('\n')
    inventory = list(map(int, rows[1].split(':')[1].split(',')))
    divisor = int(rows[3].split(' ')[-1])
    op = rows[2].split(' ')[-2:]
    items[idx] = inventory
    activity[idx] = 0
    divisors[idx] = divisor
    ops[idx] = op
    dest_true[idx] = int(rows[4].split(' ')[-1])
    dest_false[idx] = int(rows[5].split(' ')[-1])


prod = 1
for val in divisors.values():
    prod *= val

print(prod)
def round():
    for idx in range(len(data)):
        while items[idx]:
            worry = items[idx].pop(0)
            print(idx, worry)
            worry %= prod
            if ops[idx][1] == 'old':
                worry **= 2
            elif ops[idx][0] == '+':
                worry += int(ops[idx][1])
            else:
                worry *= int(ops[idx][1])
            if worry % divisors[idx] == 0:
                dest = dest_true[idx]
            else:
                dest = dest_false[idx]
            items[dest].append(worry)
            activity[idx] += 1

for r in range(1, 10001):
    round()
    if r in [1, 20, 1000, 2000, 3000]:
        print(activity)

print(activity)

acts = sorted(activity.values())[-2:]

answer = acts[0]*acts[1]
print(answer)
