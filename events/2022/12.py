from collections import deque


with open('./input_data/day_12.txt', 'r') as f:
    data = f.read().strip()

data = data.split('\n')

""" Example
data = '''Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi'''.split('\n')
"""

R = len(data)
C = len(data[0])

source = None
dest = None

# Convert to numeric:
for i in range(R):
    data[i] = list(data[i])

for i in range(R):
    for j in range(C):
        if data[i][j] == 'S':
            source = (i, j)
            data[i][j] = 'a'
        if data[i][j] == 'E':
            dest = (i, j)
            data[i][j] = 'z'
        data[i][j] = ord(data[i][j]) - ord('a')

print(source, dest)


def bfs(root, goal):
    Q = deque()
    visited = set()
    parent = {}
    Q.append(root)
    while Q:
        v = Q.popleft()
        if v == goal:
            return parent
        x,y = v
        for x2, y2 in [(x+1,y), (x-1,y), (x,y+1), (x,y-1)]:
            if not (0<=x2<R and 0<=y2<C and data[x2][y2] - data[x][y] <= 1):
                continue
            w = (x2, y2)
            if w not in visited:
                visited.add(w)
                parent[w] = v
                Q.append(w)
    

def len_shortest(root, goal):
    parents = bfs(root, goal)
    if not parents:
        return 100000000
    d = 0
    u = goal
    if root == (0,6):
        print(parents)
    while u != root:
        d += 1
        u = parents[u]
    return d

# Part 1:
answer = len_shortest(source, dest)
print(answer)

min_d = 1000000
for i in range(R):
    for j in range(C):
        if data[i][j] == 0:
            print((i,j))
            d = len_shortest( (i,j), dest)
            if d < min_d:
                min_d = d

print(min_d)
