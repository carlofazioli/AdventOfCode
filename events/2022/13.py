with open('./input_data/day_13.txt', 'r') as f:
    data = f.read().strip()

"""
data = '''[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]'''
"""


pairs = data.split('\n\n')


def compare(a, b):
    if isinstance(a, int) and isinstance(b, int):
        if a < b:
            return True
        if a == b:
            return 'continue'
        return False
    if isinstance(a, list) and isinstance(b, list):
        while a and b:
            i = a.pop(0)
            j = b.pop(0)
            result = compare(i, j)
            if result == 'continue':
                continue
            return result
        if not a and not b:
            return 'continue'
        return len(a) < len(b)
    if isinstance(a, int):
        a = [a]
    if isinstance(b, int):
        b = [b]
    return compare(a, b)

answer = 0
for i, pair in enumerate(pairs):
    pair = pair.split('\n')
    L = eval(pair[0])
    R = eval(pair[1])
    if compare(list(L), list(R)):
        answer += i+1
        print('*******************')
        print(i+1)
        print('*******************')


print(answer)


packets = []
packets.append('[[6]]')
packets.append('[[2]]')
input()
for pair in pairs:
    pair = pair.split('\n')
    L = pair[0]
    R = pair[1]
    packets.append(L)
    packets.append(R)

for p in packets:
    print(p)



input()
i = 1
while i < len(packets):
    j = i
    a = eval(packets[j])
    b = eval(packets[j-1])
    while j>0 and compare(a, b):
        tmp = packets[j]
        packets[j] = packets[j-1]
        packets[j-1] = tmp
        j -= 1
        a = eval(packets[j])
        b = eval(packets[j-1])
    i += 1

print('[[2]]' in packets)
print('[[6]]' in packets)

answer = 1
for i, p in enumerate(packets):
    if p == '[[2]]' or p == '[[6]]':
        answer *= i+1
        print('ehllo')

print(answer)
