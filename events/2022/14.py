with open('./input_data/day_14.txt', 'r') as f:
    data = f.read().strip()

"""
data = '''498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9'''
"""

data = data.split('\n')

max_y = -1
occupied = set()
for row in data:
    coords = row.split(' -> ')
    for p1, p2 in zip(coords[:-1],coords[1:]):
        x1,y1 = list(map(int, p1.split(',')))
        x2,y2 = list(map(int, p2.split(',')))
        if y1 > max_y:
            max_y = y1
        if y2 > max_y:
            max_y = y2
        if x1 == x2:
            # Vertical segment
            for y in range(min(y1, y2), max(y1, y2)+1):
                occupied.add( (x1, y) )
        if y1 == y2:
            # Horizontal segment
            for x in range(min(x1, x2), max(x1, x2)+1):
                occupied.add( (x, y1) )

def simulate(occupied):
    x,y = 500, 0
    at_rest = False
    while not at_rest:
        if y == max_y+1:
            # We are on the floor.
            pass
        elif (x, y+1) not in occupied:
            # If there's nothing below, just move there.
            y = y+1
            continue
        elif (x-1, y+1) not in occupied:
            # There IS something below.  But not down/left:
            x,y = x-1, y+1
            continue
        elif (x+1, y+1) not in occupied:
            # There is occupied below and down/left, but not down/right
            x,y = x+1, y+1
            continue
        at_rest = True
    occupied.add( (x,y) )
    return occupied, (x,y)



start = len(occupied)
old = 0
new = len(occupied)
while old < new:
    old = len(occupied)
    occupied, loc = simulate(occupied)
    new = len(occupied)
    print(loc)
    if loc == (500, 0):
        break

answer = new - start
print(answer)
