from math import inf


with open('./input_data/day_15.txt', 'r') as f:
    data = f.read().strip()
target_y = 2000000
M = 4000000

"""
data = '''Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3'''
target_y = 10
M = 20
"""

data = data.split('\n')


sensors = set()
beacons = set()

xmin = inf
xmax = -inf
dmax = -inf

for row in data:
    s,b = row.split(': closest beacon is at ')
    s = s.split('Sensor at ')
    s = s[1].split(', ')
    sx = int(s[0].split('=')[1])
    sy = int(s[1].split('=')[1])
    b = b.split(', ')
    bx = int(b[0].split('=')[1])
    by = int(b[1].split('=')[1])
    d = abs(sx-bx) + abs(sy-by)
    sensors.add( ((sx,sy), d) )
    beacons.add( (bx,by) )
    if sx < xmin: xmin = sx
    if sx > xmax: xmax = sx
    if bx < xmin: xmin = bx
    if bx > xmax: xmax = bx
    if d > dmax: dmax = d

'''
answer = 0
for x in range(xmin-dmax, xmax+dmax+1):
    candidate = True
    for loc, dist in sensors:
        d = abs(loc[0]-x) + abs(loc[1]-target_y)
        candidate &= d>dist
    if not candidate and (x,target_y) not in beacons:
        answer += 1

print(answer)
'''


'''
From the statement of the problem, we can infer that there is one
unique cell in the MxM region that is outside the manhattan distance
diamond surrounding each sensor.  To find it, we'll check only cells
that lie 1 unit farther away.
'''

for loc, dist in sensors:
    x,y = loc
    d = dist+1
    for dx in range(-d, d+1):
        dy = d - abs(dx)
        xx = x + dx
        yy = y + dy
        if not (0<=xx<=M and 0<=yy<=M):
            continue
        candidate = True
        for loc2, dist2 in sensors:
            sx,sy = loc2
            delta = abs(xx-sx) + abs(yy-sy)
            candidate &= (delta > dist2)
            if not candidate:
                break
        if candidate:
            answer = M*xx+yy
            print(xx, yy, answer)
            break
    if candidate:
        break

print(answer)
