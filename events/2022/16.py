from collections import deque
from datetime import datetime


with open('./input_data/day_16.txt', 'r') as f:
    data = f.read().strip()

"""
data = '''Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II'''
"""


data = data.split('\n')


adj = {}
rates = {}
for row in data:
    row = row.split('; ')
    valve = row[0].split(' ')[1]
    rate = int(row[0].split('=')[1])
    if 'tunnels' in row[1]:
        row = row[1].split('valves ')[1].split(', ')
    else:
        row = [row[1].split('valve ')[1]]
    adj[valve] = row
    rates[valve] = rate

# Typical breadth-first search to find the travel time between valves:
def bfs_len(r, g):
    Q = deque()
    visited = set()
    parents = {}
    Q.append(r)
    while Q:
        v = Q.popleft()
        if v == g:
            break
        for w in adj[v]:
            if w in visited:
                continue
            visited.add(w)
            parents[w] = v
            Q.append(w)
    # Today we only ever want the distance between the root and goal,
    # so let's just compute it and return the int dist:
    dist = 0
    route = []
    u = g
    while u != r:
        route = [u] + route
        dist += 1
        u = parents[u]
    return dist, route


# Keys in the distance matrix are concatenations of individual valve 
# names.  We waste the space by storing the full matrix and not just
# the symmetric half, so that we can do lookups with valves listed in
# either order.  
distances = {}
routes = {}
for n1 in adj.keys():
    for n2 in adj.keys():
        if n1 == n2:
            distances[n1+n2] = 0
            routes[n1+n2] = [n1]
            continue
        distances[n1+n2], routes[n1+n2] = bfs_len(n1, n2)


# Let's try DFS with "next valve" as the choice:
T_LIMIT = 30

t = 0
loc = 'AA'
score = 0
closed = tuple(v for v in adj.keys() if rates[v])
state = (t, loc, score, closed)

best = -1
memo = set()
values = dict()

stack = deque()
stack.append(state)

print(datetime.now())
while stack:
    s = stack.pop()
    if s in memo:
        continue
    memo.add(s)
    t, loc, score, closed = s
    for dest in closed:
        new_t = t+distances[loc+dest]+1
        if new_t >= T_LIMIT: continue
        new_loc = dest
        new_score = score + (T_LIMIT-new_t)*rates[dest]
        if best < new_score: 
            best = new_score
        new_closed = list(closed)
        new_closed.remove(dest)
        new_closed = tuple(new_closed)
        new_state = (new_t, new_loc, new_score, new_closed)
        stack.append( new_state )
print(datetime.now())

print(best)


"""
This is directly inspired by Juan Lopes' code, found here:
https://github.com/juanplopes/advent-of-code-2022/blob/main/day16.py


We parse the data one time to extract problem information.  G is a
dictionary keyed by valve name, whose values are sets of adjacent 
valves.  F is a dictionary keyed by same, and whose values are the
integer rates of the valve.  F only contains valves with nonzero
rates.
"""
G = {}
F = {}
for row in data:
    row = row.split('; ')
    valve = row[0].split(' ')[1]
    rate = int(row[0].split('=')[1])
    if 'tunnels' in row[1]:
        row = set(row[1].split('valves ')[1].split(', '))
    else:
        row = set([row[1].split('valve ')[1]])
    G[valve] = row
    if rate:
        F[valve] = rate

"""
We store the valve states as an integer.  The i-th bit of the int is
set to 1 if that valve has been opened.  This allows for fast state
updating with a single OR operation.  We facilitate this mechanism
with an index dictionary I, keyed by valve and whose values are the
individual ints representing one particular bit set. E.g.:
    I = {
        'AA': 1 (0b0000000000000001),
        'BB': 2 (0b0000000000000010),
        'CC': 4 (0b0000000000000100),
        ...
    }

So, a game state of 21 = 0b0000000000010101 would imply that the valves
with indices 0, 2, and 4 were opened.  
"""
I = {v: 1<<i for i,v in enumerate(F)}

"""
We can form the distance matrix in O(n^3).  First, we create an 
initial approximation by setting the distance between adjacent valves
to 1, and non-adjacent valves' default distance infinity.

Then, we iterate over candidate intermediate valves k.  For each such
candidate intermediate valve we see if the distance between valves 
u,v can be shortened by going through k.  This is the O(n^3) portion.

I don't have a super strong intuition about why this works, but you can
see the machinery in action if you just do a small example by hand.  
"""
T = {v: {u:1 if u in G[v] else float('+inf') for u in G} for v in G}
for k in T:
    for u in T:
        for v in T:
            T[u][v] = min(T[u][v], T[u][k]+T[k][v])


"""
The ideas here include:
  1.  The order we visit the states doesn't matter so much as how
      much time the valve is open.
  2.  If we get a better score via another route, that's the 
      effective score of that state regardless of route.  

We'll initially be at loc='AA', with 30 or 26 min remaining, and the
state int will be 0 (since no valves have opened yet); the memo will
be an empty dict.  In the end memo will store a value for each state
visited.  

This recursive solution implements a DFS, using the python 
interpreter's call stack.
"""
def solve(loc, t_remaining, state, score, memo):
    # Check the value of the current state against the best so far:
    memo[state] = max(memo.get(state, 0), score)
    for v in F:  # For each valve with a nonzero rate:
        # For the destination v, it takes T[loc][v] minutes to travel
        # there, and 1 minute to open the valve, resulting in fewer
        # minutes remaining in the game.  We can skip this destination
        # if it already open, or if the remaining time is <=0.
        new_t = t_remaining - T[loc][v] - 1
        if I[v] & state or new_t <= 0: continue
        # If we don't wanna skip this destination, let's see how we do
        # by going there next:
        solve( v, new_t, state | I[v], score + new_t*F[v], memo)
    return memo

print(datetime.now())
part1 = solve('AA', 30, 0, 0, {})
print(datetime.now())
answer1 = max(part1.values())
print(answer1)

"""
By storing the state as an int, it also makes it easy to identify two
states that have no open valves in common.  Thinking carefully allows
one to see that the answer for part two is the best score that can be
obtained by two summing two states' scores that have no open valves in
common.  

As a thought experiment to gain insight: imaging that you send the 
elephant off to open valves for 26 minutes, and then you reset the 
clock and send yourself on a run to open valves for 26 minutes.  As 
long as the set of valves you each open is non-overlapping, then both
of y'all's runs could have been executed simultaneously.  
"""
print(datetime.now())
part2 = solve('AA', 26, 0, 0, {})
answer2 = max(p1+p2 for s1,p1 in part2.items() 
                    for s2,p2 in part2.items() if not s1 & s2)
print(datetime.now())
print(answer2)


"""
The following code is my attempt at part 2.  I had it yielding the
correct answer to the example.  But, it wasn't fast enough to finish
on the problem data.  In trying to optimize, I porkchopped something
and now it doesn't even give the right answer for the example data.  
"""

# T_LIMIT = 26
# 
# t0 = 0
# t1 = 0
# loc0 = 'AA'
# loc1 = 'AA'
# score = 0
# closed = tuple(v for v in adj.keys() if rates[v])
# state = (t0, t1, loc0, loc1, score, closed)
# 
# def dedupe(t0, t1, loc0, loc1):
#     if t0 == t1:
#         loc0, loc1 = min(loc0, loc1), max(loc0, loc1)
#     elif t0 > t1:
#         t0, t1 = t1, t0
#         loc0, loc1 = loc0, loc1
#     return t0, t1, loc0, loc1
# 
# def bound(state):
#     t0, t1, loc0, loc1, score, closed = state
#     for v in closed:
#         d0 = distances[loc0+v]+1
#         d1 = distances[loc1+v]+1
#         score += (T_LIMIT-min(d0, d1))*rates[v]
#     return score
# 
# best = -1
# memo = set()
# 
# stack = deque()
# stack.append(state)
# 
# start = datetime.now()
# while stack:
#     s = stack.pop()
#     if s in memo:
#         continue
#     memo.add(s)
#     t0, t1, loc0, loc1, score, closed = s
#     if t0 >= T_LIMIT-2 and t1 >= T_LIMIT-2:
#         continue
#     for dest in closed:
#         if t0 <= t1:
#             new_t0 = t0+distances[loc0+dest]+1
#             new_t1 = t1
#             new_loc0 = dest
#             new_loc1 = loc1
#             new_score = score + (T_LIMIT-new_t0)*rates[dest]
#             new_closed = list(closed)
#             new_closed.remove(dest)
#             new_closed = tuple(new_closed)
#             new_state = (*dedupe(new_t0, new_t1, new_loc0, new_loc1),
#                          new_score,
#                          new_closed)
#             if bound(new_state) >= best:
#                 stack.append(new_state)
#         if t1 == t0:
#             # If the times are equal, we want to try having player 1
#             # move to the next dest also.
#             # Here player 1 moves:
#             new_t0 = t0
#             new_t1 = t1+distances[loc1+dest]+1
#             new_loc0 = loc0
#             new_loc1 = dest
#             new_score = score + (T_LIMIT-new_t1)*rates[dest]
#             new_closed = list(closed)
#             new_closed.remove(dest)
#             new_closed = tuple(new_closed)
#             new_state = (*dedupe(new_t0, new_t1, new_loc0, new_loc1),
#                          new_score,
#                          new_closed)
#             if bound(new_state) >= best:
#                 stack.append(new_state)
#         if best < new_score: 
#             best = new_score
# finish = datetime.now()
