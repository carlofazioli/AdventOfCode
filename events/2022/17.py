"""
The typical way to use this program is to run:
    python -i <day>.py

Doing so will automatically look for and load a file <day>.in, assumed
to be previously downloaded.  If that file is missing, this script will
grab it.  

Sometimes, it's desirable to run against the example data from that
day.  To do so, use the command:
    python -i <day>.py example.in
"""


import pdb
import sys


DAY = sys.argv[0].split('.')[0]
FILE = f'./{DAY}.in'
if len(sys.argv) > 1:
    FILE = f'./{sys.argv[1]}'

with open(FILE, 'r') as f:
    data = f.read().strip()

###
# End preamble template.  
###


def print_chamber(C, R, X, Y):
    for y in range(Y+1,0,-1):
        s = '|'
        for x in range(0, 7):
            if (x-X,y-Y) in R:
                s += '@'
            elif (x,y) in C:
                s += '#'
            else:
                s += '.'
        s += '|'
        print(s)
    print('+-------+')


rocks = [
    {(0,0), (1,0), (2,0), (3,0)},
    {(1,0), (0,-1), (1,-1), (2,-1), (1,-2)},
    {(2,0), (2,-1), (0,-2), (1,-2), (2,-2)},
    {(0,0), (0,-1), (0,-2), (0,-3)},
    {(0,0), (0,-1), (1,0), (1,-1)}
]
r = len(rocks)

chamber = set()
top = 0

N = 2022
c = 0
for i in range(N):
    rock = rocks[i%r]
    w = max(x for x,y in rock)+1
    h = max(abs(y) for x,y in rock)+1
    X,Y = 2, top+3+h
    z = 0
    while True:
        ch = data[c % len(data)]
        c += 1
        dX = 2*(ch=='>')-1
        if 0 <= X+dX < 8-w:
            # The rock is bounds.  Does it collide with the chamber?
            if not any((X+dX+x,Y+y) in chamber for x,y in rock):
                X += dX
        if any((X+x,Y+y-1) in chamber or Y+y-1<=0 for x,y in rock):
            for x,y in rock:
                top = max(Y+y, top)
                chamber.add((X+x, Y+y))
            break
        Y -= 1
        z = 0

answer1 = top
print(answer1)



N = 1000000000000

chamber = set()

profile = (0, 0, 0, 0, 0, 0, 0, 0)
top = 0
seen = {}
scores = {}


c = 0

for i in range(N):
    rock = rocks[i%r]
    w = max(x for x,y in rock)+1
    h = max(abs(y) for x,y in rock)+1
    X,Y = 2, top+3+h
    z = 0
    while True:
        ch = data[c]
        c += 1
        c %= len(data)
        dX = 2*(ch=='>')-1
        if 0 <= X+dX < 8-w:
            # The rock is bounds.  Does it collide with the chamber?
            if not any((X+dX+x,Y+y) in chamber for x,y in rock):
                X += dX
        if any((X+x,Y+y-1) in chamber or Y+y-1<=0 for x,y in rock):
            for x,y in rock:
                top = max(Y+y, top)
                chamber.add((X+x, Y+y))
            profile = [0]*7
            for x,y in chamber:
                profile[x] = max(y, profile[x])
            M = min(profile)
            profile = [p-M for p in profile]
            profile.append(i%r)
            profile = tuple(profile)
            break
        Y -= 1
    if profile not in seen:
        seen[profile] = i
        scores[i] = top
    else:
        scores[i] = top
        print('found repeat!')
        print(f'previous i={seen[profile]}, current {i=}')
        print(f'{profile=}')
        old_i = seen[profile]
        old_top = scores[old_i]
        delta = top-old_top
        period = i - old_i
        loops = (N-old_i)//period
        answer2 = old_top + delta*loops
        remaining = N - (old_i + period*loops) - 1
        answer2 += scores[remaining+old_i]-scores[old_i]
        print(f'computed {answer2=}; true answer2=1514285714288; equal={answer2==1514285714288}')
        break
    z = 0


print(answer2)
z=1
